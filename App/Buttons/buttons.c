/***********************************************************************
 * uart.c
 *
 *  Created on: 14 sty 2021
 *      Author: Dawid
 **********************************************************************/

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include <avr/io.h>
#include <stdint.h>
#include <util/delay.h>

#include "buttons.h"
#include "common.h"

#include "logger.h"
/***********************************************************************
 * DEFINES
 **********************************************************************/

#define BTN_COLOR_PORT		(volatile uint8_t*)(0x08 + 0x20)	/* PORTC */
#define BTN_COLOR_PORT_IN	(volatile uint8_t*)(0x06 + 0x20)	/* PINC */
#define BTN_COLOR_DDR		(volatile uint8_t*)(0x07 + 0x20)	/* DDRC */

#define BTN_BLUE_PIN		PC0
#define BTN_RED_PIN			PC1
#define BTN_WHITE_PIN		PC2
#define BTN_GREEN_PIN		PC3



#define BTN_FUNC_PORT		(volatile uint8_t*)(0x05 + 0x20)	/* PORTB */
#define BTN_FUNC_PORT_IN	(volatile uint8_t*)(0x03 + 0x20)	/* PINB */
#define BTN_FUNC_DDR		(volatile uint8_t*)(0x04 + 0x20)	/* DDRB */

#define BTN_START_PIN		PB4
#define BTN_REPEAT_PIN		PB5




#define TIMER1_PRESC			(1024U)
#define DEBOUNCING_TIME 	(5U)	// 5ms of button debouncing
#define TIMER_IRQ_FREQ		(1000U  /DEBOUNCING_TIME)
#define OCR_VAL				((uint8_t)((F_CPU / TIMER1_PRESC / TIMER_IRQ_FREQ) - 1U))

#define BTNS_COLOR_ALL_RELEASED		(0x0Fu)
#define BTNS_COLOR_STATE_MASK		(0x0Fu)

#define BTNS_FUNC_ALL_RELEASED		(0x30u)
#define BTNS_FUNC_STATE_MASK		(0x30u)

/***********************************************************************
 * TYPES
 **********************************************************************/

/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/

/***********************************************************************
 * LOCAL DATA
 **********************************************************************/
tButtonInfo colorButtons[COLOR_BUTTONS_NUM] =
{
	{ BTN_COLOR_DDR, BTN_COLOR_PORT, BTN_COLOR_PORT_IN, BTN_BLUE_PIN, 	State_Released, State_Released, LED_BLUE,  Song_FA  },
	{ BTN_COLOR_DDR, BTN_COLOR_PORT, BTN_COLOR_PORT_IN, BTN_RED_PIN, 	State_Released, State_Released, LED_RED,   Song_MI  },
	{ BTN_COLOR_DDR, BTN_COLOR_PORT, BTN_COLOR_PORT_IN, BTN_WHITE_PIN, 	State_Released, State_Released, LED_WHITE, Song_RE  },
	{ BTN_COLOR_DDR, BTN_COLOR_PORT, BTN_COLOR_PORT_IN, BTN_GREEN_PIN, 	State_Released, State_Released, LED_GREEN, Song_DO  },
};

tButtonInfo funcButtons[FUNC_BUTTONS_NUM] =
{
	{ BTN_FUNC_DDR, BTN_FUNC_PORT, BTN_FUNC_PORT_IN, BTN_START_PIN,  State_Released, State_Released, LED_START  },
	{ BTN_FUNC_DDR, BTN_FUNC_PORT, BTN_FUNC_PORT_IN, BTN_REPEAT_PIN, State_Released, State_Released, LED_REPEAT },
};

volatile tBool buttonColorIrq = false;
volatile tBool buttonFuncIrq = false;
/***********************************************************************
 * PROTOTYPES
 **********************************************************************/
static void InitializeTimer(void);
static void EnableTimer(void);
static void DisableTimer(void);

static uint8_t GetIrqReasonBtn_Color(uint8_t current_state, uint8_t prev_state);
static void HandleColorBtnInterrupt(void);
static void HandleFuncBtnInterrupt(void);
static void SetButtonState(tButtonInfo* button);

/***********************************************************************
 * FUNCTIONS
 **********************************************************************/

void Buttons_Init(void)
{
	*(BTN_COLOR_DDR) &= ~((1 << BTN_BLUE_PIN) | (1 << BTN_WHITE_PIN) | (1 << BTN_RED_PIN) | (1 << BTN_GREEN_PIN));
	*(BTN_COLOR_PORT) |=  (1 << BTN_BLUE_PIN) | (1 << BTN_WHITE_PIN) | (1 << BTN_RED_PIN) | (1 << BTN_GREEN_PIN);

	*(BTN_FUNC_DDR) &= ~((1 << BTN_START_PIN) | (1 << BTN_REPEAT_PIN));
	*(BTN_FUNC_PORT) |=  (1 << BTN_START_PIN) | (1 << BTN_REPEAT_PIN);

	PCMSK1 |= (1 << PCINT8) | (1 << PCINT9) | (1 << PCINT10) | (1 << PCINT11);
	PCMSK0 |= (1 << PCINT4) | (1 << PCINT5);

	InitializeTimer();
	Button_EnableBtnInterrupts(COLOR_BUTTONS_IRQ_SECTION);
	Button_EnableBtnInterrupts(FUNC_BUTTONS_IRQ_SECTION);
}

tButtonState Buttons_GetState(tButton button)
{
	tButtonState button_state = State_Released;

	if (button < COLOR_BUTTONS_NUM)
	{
		button_state = colorButtons[button].currentState;
	}
	else
	{
		button_state = funcButtons[button - COLOR_BUTTONS_NUM].currentState;
	}

	return button_state;
}

void Button_EnableBtnInterrupts(uint8_t section)
{
	PCICR |= (1 << section);
}

void Button_DisableBtnInterrupts(uint8_t section)
{
	PCICR &= ~(1 << section);
}

static void InitializeTimer(void)
{
	TCCR0A |= (1 << WGM01);
	OCR0A = OCR_VAL;
}

static void EnableTimer(void)
{
	TCNT0 = 0u;
	TCCR0B |= (1 << CS00) | (1 << CS02);
	TIMSK0 |= (1 << OCIE0A);
}

static void DisableTimer(void)
{
	TCCR0B &= ~((1 << CS00) | (1 << CS01) | (1 << CS02));
	TIMSK0 |= (1 << OCIE0A);
}

static void SetButtonState(tButtonInfo* button)
{
	button->currentState = (tButtonState)((*button->port_in & (1 << button->pin)) == (1 << button->pin));
}

static uint8_t GetIrqReasonBtn_Color(uint8_t current_state, uint8_t prev_state)
{
	uint8_t btn_idx = 0u;
	const uint8_t change = current_state ^ prev_state;

	for (uint8_t idx =0; idx < COLOR_BUTTONS_NUM; idx++)
	{
		if ((1 << colorButtons[idx].pin) == change)
		{
			btn_idx = idx;
		}
	}

	return btn_idx;
}

static uint8_t GetIrqReasonBtn_Func(uint8_t current_state, uint8_t prev_state)
{
	uint8_t btn_idx = 0u;
	const uint8_t change = current_state ^ prev_state;

	for (uint8_t idx =0; idx < FUNC_BUTTONS_NUM; idx++)
	{
		if ((1 << funcButtons[idx].pin) == change)
		{
			btn_idx = idx;
		}
	}

	return btn_idx;
}

static void HandleColorBtnInterrupt(void)
{
	static uint8_t prev_buttons_state = BTNS_COLOR_ALL_RELEASED;
	uint8_t buttons_state = BTNS_COLOR_ALL_RELEASED;
	uint8_t current_btn_idx = 0u;

	buttons_state = (*(BTN_COLOR_PORT_IN) & BTNS_COLOR_STATE_MASK);
	current_btn_idx = GetIrqReasonBtn_Color(buttons_state, prev_buttons_state);

	SetButtonState(&colorButtons[current_btn_idx]);

	if (State_Pressed == colorButtons[current_btn_idx].currentState)
	{
		LedManager_SetLedState(colorButtons[current_btn_idx].led, ENABLE);
		Player_SetSongToPlay(colorButtons[current_btn_idx].song);
	}
	else if (State_Released == colorButtons[current_btn_idx].currentState)
	{
		LedManager_SetLedState(colorButtons[current_btn_idx].led, DISABLE);
	}

	prev_buttons_state = buttons_state;
}

static void HandleFuncBtnInterrupt(void)
{
	static uint8_t prev_buttons_state = BTNS_FUNC_ALL_RELEASED;
	uint8_t buttons_state = BTNS_FUNC_ALL_RELEASED;
	uint8_t current_btn_idx = 0u;

	buttons_state = (*(BTN_FUNC_PORT_IN) & BTNS_FUNC_STATE_MASK);
	current_btn_idx = GetIrqReasonBtn_Func(buttons_state, prev_buttons_state);

	SetButtonState(&funcButtons[current_btn_idx]);

	if (State_Pressed == funcButtons[current_btn_idx].currentState)
	{
		LedManager_SetLedState(funcButtons[current_btn_idx].led, ENABLE);
	}
	else if (State_Released == funcButtons[current_btn_idx].currentState)
	{
		LedManager_SetLedState(funcButtons[current_btn_idx].led, DISABLE);
	}
	prev_buttons_state = buttons_state;
}

ISR(PCINT1_vect)
{
	buttonColorIrq = true;
	Button_DisableBtnInterrupts(COLOR_BUTTONS_IRQ_SECTION);
	EnableTimer();
}

ISR(PCINT0_vect)
{
	buttonFuncIrq = true;
	Button_DisableBtnInterrupts(FUNC_BUTTONS_IRQ_SECTION);
	EnableTimer();
}

ISR(TIMER0_COMPA_vect)
{
	DisableTimer();

	if (buttonColorIrq)
	{
		HandleColorBtnInterrupt();
		PCIFR |= (1 << PCIF1);
		Button_EnableBtnInterrupts(COLOR_BUTTONS_IRQ_SECTION);
		buttonColorIrq = false;
	}

	if (buttonFuncIrq)
	{
		HandleFuncBtnInterrupt();
		PCIFR |= (1 << PCIF0);
		Button_EnableBtnInterrupts(FUNC_BUTTONS_IRQ_SECTION);
		buttonFuncIrq = false;
	}
}

