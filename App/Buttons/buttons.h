/***********************************************************************
 * buttons.h
 *
 *  Created on: 28 sty 2021
 *      Author: Dawid
 **********************************************************************/

#ifndef BUTTONS_BUTTONS_H_
#define BUTTONS_BUTTONS_H_

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include <avr/interrupt.h>
#include "led_manager.h"
#include "mp3player.h"
/***********************************************************************
 * DEFINES
 **********************************************************************/
#define COLOR_BUTTONS_IRQ_SECTION	(PCIE1)
#define FUNC_BUTTONS_IRQ_SECTION	(PCIE0)

#define COLOR_BUTTONS_NUM			(4u)
#define FUNC_BUTTONS_NUM			(2u)

/***********************************************************************
 * TYPES
 **********************************************************************/
typedef enum
{
	State_Pressed,
	State_Released,
}tButtonState;

typedef struct
{
	tButtonState current_state;
	tButtonState previous_state;
}tButtonChange;

typedef enum
{
	Button_Blue,
	Button_Red,
	Button_White,
	Button_Green,
	Button_Start,
	Button_Repeat
}tButton;

typedef struct
{
	volatile uint8_t* ddr;
	volatile uint8_t* port_out;
	volatile uint8_t* port_in;
	const uint8_t 	  pin;
	tButtonState	  currentState;
	tButtonState 	  prevState;
	const tLed 		  led;
	const tPlayerSong song;
}tButtonInfo;

/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/
extern tButtonInfo colorButtons[COLOR_BUTTONS_NUM];
extern tButtonInfo funcButtons[FUNC_BUTTONS_NUM];
/***********************************************************************
 * PROTOTYPES
 **********************************************************************/
void Buttons_Init(void);
tButtonState Buttons_GetState(tButton button);
void Button_EnableBtnInterrupts(uint8_t section);
void Button_DisableBtnInterrupts(uint8_t section);

#endif /* BUTTONS_BUTTONS_H_ */
