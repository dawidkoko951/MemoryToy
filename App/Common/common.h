/***********************************************************************
 * common.h
 *
 *  Created on: 3 mar 2021
 *      Author: Dawid
 **********************************************************************/

#ifndef COMMON_COMMON_H_
#define COMMON_COMMON_H_

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include <stdint.h>

/***********************************************************************
 * DEFINES
 **********************************************************************/

/***********************************************************************
 * TYPES
 **********************************************************************/
typedef enum
{
	DISABLE,
	ENABLE
}tState;

typedef enum
{
	false,
	true
}tBool;

/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/

/***********************************************************************
 * PROTOTYPES
 **********************************************************************/

#endif /* COMMON_COMMON_H_ */
