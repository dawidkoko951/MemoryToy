/***********************************************************************
 * delay.c
 *
 *  Created on: 19 mar 2021
 *      Author: Dawid
 **********************************************************************/

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include "delay.h"
#include "timer.h"
/***********************************************************************
 * DEFINES
 **********************************************************************/
#define TIMER_IRQ_FREQ	((uint16_t)1000U)	/* 1000Hz, IRQ once a 1ms */

/***********************************************************************
 * TYPES
 **********************************************************************/

/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/

/***********************************************************************
 * LOCAL DATA
 **********************************************************************/
volatile static uint32_t delayTick = 0U;
/***********************************************************************
 * PROTOTYPES
 **********************************************************************/
static void TimerISR(void);
/***********************************************************************
 * FUNCTIONS
 **********************************************************************/

void Delay_Init(void)
{
	tTimerInfo timer_A;

	timer_A.channel = Timer_A;
	timer_A.isr_freq = TIMER_IRQ_FREQ;
	timer_A.isr_callback = TimerISR;

	Timer_InitializeTim2(&timer_A);
}

uint32_t Delay_GetTicks(void)
{
	return delayTick;
}

static void TimerISR(void)
{
	delayTick++;
}
