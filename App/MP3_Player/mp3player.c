/***********************************************************************
 * mp3player.c
 *
 *  Created on: 14 sty 2021
 *      Author: Dawid
 **********************************************************************/

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include "mp3player.h"
#include "uart.h"

/***********************************************************************
 * DEFINES
 **********************************************************************/
#define VOLUME_CMD_SIZE			((uint8_t)5)
#define VOLUME_VOL_IDX			((uint8_t)3)
#define VOLUME_SM_IDX			((uint8_t)4)
#define VOLUME_MAX				((uint8_t)30)

#define PLAY_CMD_SIZE			((uint8_t)6)
#define PLAY_AUDIO_UPPER_IDX 	((uint8_t)3)
#define PLAY_AUDIO_LOWER_IDX 	((uint8_t)4)
#define PLAY_SM_IDX				((uint8_t)5)

/***********************************************************************
 * TYPES
 **********************************************************************/

/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/
volatile tPlayerSong songToPlay = 0xFF;
/***********************************************************************
 * LOCAL DATA
 **********************************************************************/

/***********************************************************************
 * PROTOTYPES
 **********************************************************************/
static uint8_t CalculateSM(uint8_t* buffer, uint8_t size);

/***********************************************************************
 * FUNCTIONS
 **********************************************************************/
void Player_Init(void)
{
	Uart_Init();

	Player_SetVolume(15);
}

void Player_SetVolume(uint8_t volume)
{
	uint8_t buffer[VOLUME_CMD_SIZE] = {0xAA, 0x13, 0x01, 0x00, 0x00};

	if (VOLUME_MAX < volume)
	{
		volume = VOLUME_MAX;
	}

	buffer[VOLUME_VOL_IDX] = volume;
	buffer[VOLUME_SM_IDX] = CalculateSM(buffer, VOLUME_CMD_SIZE - 1U);

	Uart_Write(buffer, VOLUME_CMD_SIZE);
}

void Player_PlayAudio(uint16_t audio)
{
	uint8_t buffer[PLAY_CMD_SIZE] = {0xAA, 0x07, 0x02, 0x00, 0x00, 0x00};
	buffer[PLAY_AUDIO_UPPER_IDX] = (uint8_t)(audio >> 8U);
	buffer[PLAY_AUDIO_LOWER_IDX] = (uint8_t)audio;
	buffer[PLAY_SM_IDX] = CalculateSM(buffer, PLAY_CMD_SIZE - 1U);

	Uart_Write(buffer, PLAY_CMD_SIZE);
}

void Player_SetSongToPlay(tPlayerSong song)
{
	songToPlay = song;
}

tPlayerSong Player_GetSongToPlay(void)
{
	return songToPlay;
}

static uint8_t CalculateSM(uint8_t* buffer, uint8_t size)
{
	uint8_t sm = 0x00U;

	for (uint8_t idx = 0U; idx < size; idx++)
	{
		sm += buffer[idx];
	}

	return sm;
}
