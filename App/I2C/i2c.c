/***********************************************************************
 * i2c.c
 *
 *  Created on: 20 lut 2021
 *      Author: Dawid
 **********************************************************************/

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include <avr/io.h>
#include <stdint.h>

/***********************************************************************
 * DEFINES
 **********************************************************************/
#define SDA_DDR		DDRC
#define SDA_PORT	PORTC
#define SDA_PIN		PC4

#define SCL_DDR		DDRC
#define SCL_PORT	PORTC
#define SCL_PIN		PC5

#define I2C_BIT_RATE	(100000U)	// Hz
#define I2C_PRESC		(4U)
#define TWBR_VAL		(uint8_t)(((F_CPU/I2C_BIT_RATE) - 16U) / (2U * I2C_PRESC))

#define TWSR_MASK		(0xF8u)
#define TWSR_START		(0x08u)
#define TWSR_SLA_W_ACK	(0x18u)
#define TWSR_DATA_W_ACK (0x28u)

#define RW_IDX			(0u)
#define READ_VAL		(1u)
#define WRITE_VAL		(0u)

/***********************************************************************
 * TYPES
 **********************************************************************/

/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/

/***********************************************************************
 * LOCAL DATA
 **********************************************************************/

/***********************************************************************
 * PROTOTYPES
 **********************************************************************/
static void SendStart(void);
static void SendSLA(uint8_t address);
static void SendData(uint8_t data);
static void SendStop(void);

/***********************************************************************
 * FUNCTIONS
 **********************************************************************/

void I2C_Init(void)
{
	DDRC  |= (1 << SDA_PIN) | (1 << SCL_PIN);
	TWSR |= (1 << TWPS0);	// PRESC = 4
	TWBR = TWBR_VAL;

	//TWCR = (1 << TWEN);	// Enable TWI
}

void I2C_Write(uint8_t slave_addr, uint8_t data)
{
	SendStart();

	const uint8_t sla_w = (slave_addr << 1) | (WRITE_VAL << RW_IDX);
	SendSLA(sla_w);
	SendData(data);
	SendStop();
}

static void SendStart(void)
{
	TWCR = (1 << TWSTA) | (1 << TWINT) | (1 << TWEN);
	while(!(TWCR & (1 << TWINT)));
}

static void SendSLA(uint8_t address)
{
	if ((TWSR & TWSR_MASK) == TWSR_START)
	{
		TWDR = address;
		TWCR = (1 << TWINT) | (1 << TWEN);
		while(!(TWCR & (1 << TWINT)));
	}
}

static void SendData(uint8_t data)
{
	if ((TWSR & TWSR_MASK) == TWSR_SLA_W_ACK)
	{
		TWDR = data;
		TWCR = (1 << TWINT) | (1 << TWEN);
		while(!(TWCR & (1 << TWINT)));
	}
}

static void SendStop(void)
{
	if ((TWSR & TWSR_MASK) == TWSR_DATA_W_ACK)
	{
		TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN);
	}
}
