/***********************************************************************
 * timer.c
 *
 *  Created on: 19 mar 2021
 *      Author: Dawid
 **********************************************************************/

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stddef.h>

#include "timer.h"
#include "logger.h"
/***********************************************************************
 * DEFINES
 **********************************************************************/
#define TIMER1_PRESC		(64U)
#define TIMER2_PRESC		(64U)
/***********************************************************************
 * TYPES
 **********************************************************************/

/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/

/***********************************************************************
 * LOCAL DATA
 **********************************************************************/
void (*tim1_isrA_callback)(void) = NULL;
void (*tim1_isrB_callback)(void) = NULL;
void (*tim2_isrA_callback)(void) = NULL;
void (*tim2_isrB_callback)(void) = NULL;
/***********************************************************************
 * PROTOTYPES
 **********************************************************************/

/***********************************************************************
 * FUNCTIONS
 **********************************************************************/
void Timer_InitializeTim1(tTimerInfo* userInfo)
{
	/* Prescaler = 64, CTC mode */
	TCCR1B |= (1 << WGM12) | (1 << CS10) | (1 << CS11);

	switch(userInfo->channel)
	{
		case Timer_A:
		{
			TIMSK1 |= (1 << OCIE1A);
			tim1_isrA_callback = userInfo->isr_callback;

			OCR1A = (F_CPU / TIMER1_PRESC / userInfo->isr_freq) - 1U;
			break;
		}
		case Timer_B:
		{
			TIMSK1 |= (1 << OCIE1B);
			tim1_isrB_callback = userInfo->isr_callback;

			OCR1B = (F_CPU / TIMER1_PRESC / userInfo->isr_freq) - 1U;
			break;
		}
		default:
		{
			break;
		}
	}
}

void Timer_InitializeTim2(tTimerInfo* userInfo)
{
	TCCR2A |= (1 << WGM21);
	TCCR2B |= (1 << CS22);

	switch(userInfo->channel)
	{
		case Timer_A:
		{
			TIMSK2 |= (1 << OCIE2A);
			tim2_isrA_callback = userInfo->isr_callback;

			OCR2A = (F_CPU / TIMER2_PRESC / userInfo->isr_freq) - 1U;
			break;
		}
		case Timer_B:
		{
			TIMSK2 |= (1 << OCIE2B);
			tim2_isrB_callback = userInfo->isr_callback;

			OCR2B = (F_CPU / TIMER2_PRESC / userInfo->isr_freq) - 1U;
			break;
		}
		default:
		{
			break;
		}
	}
}

ISR(TIMER1_COMPA_vect)
{
	if (NULL != tim1_isrA_callback)
	{
		tim1_isrA_callback();
	}
}

ISR(TIMER2_COMPA_vect)
{
	if (NULL != tim2_isrA_callback)
	{
		tim2_isrA_callback();
	}
}
