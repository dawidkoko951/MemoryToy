/***********************************************************************
 * fsm_timer.h
 *
 *  Created on: 8 kwi 2021
 *      Author: Dawid
 **********************************************************************/

#ifndef TOY_MANAGER_FSM_TIMER_H_
#define TOY_MANAGER_FSM_TIMER_H_

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include <stdint.h>
#include "common.h"
/***********************************************************************
 * DEFINES
 **********************************************************************/

/***********************************************************************
 * TYPES
 **********************************************************************/
typedef struct
{
	tBool isRunning;
	uint32_t startTime;
	uint32_t timeout;
}tFsmTimer;

typedef enum
{
	Timer_NotOk,
	Timer_Ok
}tFsmTimerResult;
/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/

/***********************************************************************
 * PROTOTYPES
 **********************************************************************/
tFsmTimerResult FsmTimer_StartTimer(tFsmTimer * const timer_info);
tBool FsmTimer_IsTimerExpired(tFsmTimer * const timer_info);
void FsmTimer_ResetTimer(tFsmTimer * const timer_info);

#endif /* TOY_MANAGER_FSM_TIMER_H_ */
