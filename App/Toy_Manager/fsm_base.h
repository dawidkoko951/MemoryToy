/***********************************************************************
 * fsm_base.h
 *
 *  Created on: 12 mar 2021
 *      Author: Dawid
 **********************************************************************/

#ifndef TOY_MANAGER_FSM_BASE_H_
#define TOY_MANAGER_FSM_BASE_H_

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include "buttons.h"
#include "mp3player.h"
#include "common.h"
/***********************************************************************
 * DEFINES
 **********************************************************************/
#define MAX_BUTTONS_IN_SEQ	(100u)

#define NO_PRESSED_BUTTON	 (0xAA)
#define PRESSED_BUTTON	 	 (0u)
/***********************************************************************
 * TYPES
 **********************************************************************/
typedef enum
{
	FsmState_Init,
	FsmState_Idle,
	FsmState_GenerateSeq,
	FsmState_ShowSeqEnable,
	FsmState_ShowSeqDisable,
	FsmState_WaitForUser,
	FsmState_Win,
	FsmState_Lose,
}tFsmState;

typedef enum
{
	User_NoReaction,
	User_Win,
	User_Lose,
}tUserChooseState;

typedef struct
{
	tButtonChange startButton;
	tButtonChange repeatButton;
}tFuncButtons;

typedef struct
{
	tButtonChange blueButton;
	tButtonChange redButton;
	tButtonChange whiteButton;
	tButtonChange greenButton;
}tColorButtons;

typedef struct
{
	tFsmState currentState;							// current state of the machine
	tButton lastPressedColorBtn;					// the latest pressed button
	tFuncButtons funcButton;						// information about functional buttons
	tColorButtons colorButton;						// information about color buttons
	uint16_t score;									// current score
	uint8_t currentRound;							// current round
	uint8_t buttonsNumInSeq;						// number of buttons to be shown in the current round
	tButton buttonSeqTable[MAX_BUTTONS_IN_SEQ];		// table storing buttons to be shown in the current round
	tLed buttonLed[COLOR_BUTTONS_NUM];				// leds assigned to particular buttons
	tPlayerSong buttonSong[COLOR_BUTTONS_NUM];
	uint8_t buttonToShow;							// current button to be shown in FsmState_ShowSeq
	uint8_t buttonToCheck;							// current button to be checked in FsmState_WaitForUser
	tUserChooseState userChooseState;
	tPlayerSong songToPlay;
}tFsmInfo;
/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/

/***********************************************************************
 * PROTOTYPES
 **********************************************************************/
void UpdateButtonState(tButtonChange * const button);
tButton GenerateRandomButton(void);
void InitializeButtonGenerator(void);
uint8_t CalculateButtonsInSeq(const uint8_t round);
tBool IsButtonPressed(const tButtonChange * const button_change);
uint8_t GetLastPressedButton(tFsmInfo* fsm_info);

#endif /* TOY_MANAGER_FSM_BASE_H_ */
