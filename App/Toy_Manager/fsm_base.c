/***********************************************************************
 * fsm_base.c
 *
 *  Created on: 2 kwi 2021
 *      Author: Dawid
 **********************************************************************/

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "fsm_base.h"
#include "delay.h"

/***********************************************************************
 * DEFINES
 **********************************************************************/
#define MIN_NUMBER_FOR_RAND	(0u)
#define MAX_NUMBER_FOR_RAND	(3u)

#define BUTTONS_IN_FIRST_SEQ (2u)
#define SEQ_NUM_TO_INCREASE	 (5u)

/***********************************************************************
 * TYPES
 **********************************************************************/

/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/

/***********************************************************************
 * LOCAL DATA
 **********************************************************************/

/***********************************************************************
 * PROTOTYPES
 **********************************************************************/

/***********************************************************************
 * FUNCTIONS
 **********************************************************************/

void UpdateButtonState(tButtonChange * const button)
{
	button->previous_state = button->current_state;
}

tButton GenerateRandomButton(void)
{
	return (tButton)(rand() % (MAX_NUMBER_FOR_RAND + 1u - MIN_NUMBER_FOR_RAND) - MIN_NUMBER_FOR_RAND);
}
void InitializeButtonGenerator(void)
{
	srand(Delay_GetTicks());
}

uint8_t CalculateButtonsInSeq(const uint8_t round)
{
	return BUTTONS_IN_FIRST_SEQ + (round / SEQ_NUM_TO_INCREASE);
}

tBool IsButtonPressed(const tButtonChange * const button_change)
{
	tBool pressed = false;

	if ((button_change->current_state == State_Pressed) &&
		(button_change->previous_state == State_Released))
	{
		pressed = true;
	}

	return pressed;
}

uint8_t GetLastPressedButton(tFsmInfo* fsm_info)
{
	uint8_t result = NO_PRESSED_BUTTON;
	tButton local_pressed = 0xFF;

	if (IsButtonPressed(&fsm_info->colorButton.blueButton))
	{
		local_pressed = Button_Blue;
	}
	else if (IsButtonPressed(&fsm_info->colorButton.redButton))
	{
		local_pressed = Button_Red;
	}
	else if (IsButtonPressed(&fsm_info->colorButton.whiteButton))
	{
		local_pressed = Button_White;
	}
	else if (IsButtonPressed(&fsm_info->colorButton.greenButton))
	{
		local_pressed = Button_Green;
	}

	if (0xFF != local_pressed)
	{
		fsm_info->lastPressedColorBtn = local_pressed;
		result = PRESSED_BUTTON;
	}

	return result;
}
