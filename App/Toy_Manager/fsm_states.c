/***********************************************************************
 * fsm_states.c
 *
 *  Created on: 12 mar 2021
 *      Author: Dawid
 **********************************************************************/

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include <stdint.h>
#include <util/delay.h>

#include "fsm_states.h"
#include "buttons.h"
#include "display.h"
#include "led_manager.h"
#include "fsm_timer.h"
#include "common.h"

#include "logger.h"
/***********************************************************************
 * DEFINES
 **********************************************************************/
#define SCORE_RESET_VAL		(0u)

#define SHOW_SEQ_ENABLE_DELAY	(1000)
#define SHOW_SEQ_DISABLE_DELAY	(700)
#define GENERATE_SEQ_DELAY		(1000)
/***********************************************************************
 * TYPES
 **********************************************************************/

/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/

/***********************************************************************
 * LOCAL DATA
 **********************************************************************/
static tFsmTimer fsmTimer = { .isRunning = false };
/***********************************************************************
 * PROTOTYPES
 **********************************************************************/

/***********************************************************************
 * FUNCTIONS
 **********************************************************************/

void Fsm_Init(tFsmInfo * const fsmInfo)
{
	Button_EnableBtnInterrupts(COLOR_BUTTONS_IRQ_SECTION);
	fsmInfo->score = SCORE_RESET_VAL;
	Display_SetScore(fsmInfo->score);
}

void FsmTransition_Init(tFsmInfo * const fsmInfo)
{
	fsmInfo->currentState = FsmState_Idle;
}

void Fsm_Idle(tFsmInfo * const fsmInfo)
{

}

void FsmTransition_Idle(tFsmInfo * const fsmInfo)
{
	if (IsButtonPressed(&fsmInfo->funcButton.startButton))
	{
		fsmInfo->currentState = FsmState_GenerateSeq;
	}
}

void Fsm_GenerateSeq(tFsmInfo * const fsmInfo)
{
	if (false == fsmTimer.isRunning)
	{
		InitializeButtonGenerator();
		Button_DisableBtnInterrupts(COLOR_BUTTONS_IRQ_SECTION);
		fsmInfo->buttonToShow = 0u;
		fsmTimer.timeout = GENERATE_SEQ_DELAY;
		FsmTimer_StartTimer(&fsmTimer);

		fsmInfo->buttonsNumInSeq = CalculateButtonsInSeq(fsmInfo->currentRound);

		for (uint8_t idx = 0u; idx < fsmInfo->buttonsNumInSeq; idx++)
		{
			fsmInfo->buttonSeqTable[idx] = GenerateRandomButton();
		}
	}
}

void FsmTransition_GenerateSeq(tFsmInfo * const fsmInfo)
{
	if (IsButtonPressed(&fsmInfo->funcButton.startButton))
	{
		fsmInfo->currentState = FsmState_GenerateSeq;
	}
	else if (FsmTimer_IsTimerExpired(&fsmTimer))
	{
		fsmInfo->currentState = FsmState_ShowSeqEnable;
	}
	else
	{
		fsmInfo->currentState = FsmState_GenerateSeq;
	}

	if (FsmState_GenerateSeq != fsmInfo->currentState)
	{
		FsmTimer_ResetTimer(&fsmTimer);
	}
}

void Fsm_ShowSeqEnable(tFsmInfo * const fsmInfo)
{
	if (false == fsmTimer.isRunning)
	{
		const tButton buttonIdx = fsmInfo->buttonSeqTable[fsmInfo->buttonToShow];
		Button_DisableBtnInterrupts(COLOR_BUTTONS_IRQ_SECTION);

		fsmTimer.timeout = SHOW_SEQ_ENABLE_DELAY;
		FsmTimer_StartTimer(&fsmTimer);

		LedManager_SetLedState(fsmInfo->buttonLed[buttonIdx], ENABLE);
		Player_SetSongToPlay(fsmInfo->buttonSong[buttonIdx]);
	}
}

void FsmTransition_ShowSeqEnable(tFsmInfo * const fsmInfo)
{
	if (IsButtonPressed(&fsmInfo->funcButton.startButton))
	{
		fsmInfo->currentState = FsmState_GenerateSeq;
	}
	else if (IsButtonPressed(&fsmInfo->funcButton.repeatButton))
	{
		fsmInfo->buttonToShow = 0u;
		fsmInfo->currentState = FsmState_ShowSeqEnable;
	}
	else if (FsmTimer_IsTimerExpired(&fsmTimer))
	{
		fsmInfo->currentState = FsmState_ShowSeqDisable;
	}
	else
	{
		fsmInfo->currentState = FsmState_ShowSeqEnable;
	}

	if (FsmState_ShowSeqEnable != fsmInfo->currentState)
	{
		FsmTimer_ResetTimer(&fsmTimer);
	}
}

void Fsm_ShowSeqDisable(tFsmInfo * const fsmInfo)
{
	if (false == fsmTimer.isRunning)
	{
		const tButton buttonIdx = fsmInfo->buttonSeqTable[fsmInfo->buttonToShow];

		fsmTimer.timeout = SHOW_SEQ_DISABLE_DELAY;
		FsmTimer_StartTimer(&fsmTimer);

		LedManager_SetLedState(fsmInfo->buttonLed[buttonIdx], DISABLE);
		fsmInfo->buttonToShow++;
	}
}

void FsmTransition_ShowSeqDisable(tFsmInfo * const fsmInfo)
{
	if (IsButtonPressed(&fsmInfo->funcButton.startButton))
	{
		fsmInfo->currentState = FsmState_GenerateSeq;
	}
	else if (IsButtonPressed(&fsmInfo->funcButton.repeatButton))
	{
		fsmInfo->buttonToShow = 0u;
		fsmInfo->currentState = FsmState_ShowSeqEnable;
	}
	else if (FsmTimer_IsTimerExpired(&fsmTimer))
	{
		if (fsmInfo->buttonToShow < fsmInfo->buttonsNumInSeq)
		{
			fsmInfo->currentState = FsmState_ShowSeqEnable;
		}
		else
		{
			fsmInfo->currentState = FsmState_WaitForUser;
			fsmInfo->lastPressedColorBtn = NO_PRESSED_BUTTON;
			fsmInfo->buttonToCheck = 0u;
			fsmInfo->userChooseState = User_NoReaction;
		}
	}
	else
	{
		fsmInfo->currentState = FsmState_ShowSeqDisable;
	}

	if (FsmState_ShowSeqDisable != fsmInfo->currentState)
	{
		FsmTimer_ResetTimer(&fsmTimer);
	}
}

void Fsm_WaitForUser(tFsmInfo * const fsmInfo)
{
	Button_EnableBtnInterrupts(COLOR_BUTTONS_IRQ_SECTION);

	if (NO_PRESSED_BUTTON != GetLastPressedButton(fsmInfo))
	{
		if (fsmInfo->lastPressedColorBtn == fsmInfo->buttonSeqTable[fsmInfo->buttonToCheck])
		{
			fsmInfo->userChooseState = User_Win;
			fsmInfo->buttonToCheck++;
		}
		else
		{
			fsmInfo->userChooseState = User_Lose;
			fsmInfo->buttonToCheck = 0u;
		}

		fsmInfo->lastPressedColorBtn = NO_PRESSED_BUTTON;
	}
}

void FsmTransition_WaitForUser(tFsmInfo * const fsmInfo)
{
	if (IsButtonPressed(&fsmInfo->funcButton.startButton))
	{
		fsmInfo->buttonToShow = 0u;
		fsmInfo->buttonToCheck = 0u;
		fsmInfo->currentState = FsmState_GenerateSeq;
	}
	else if (IsButtonPressed(&fsmInfo->funcButton.repeatButton))
	{
		fsmInfo->buttonToShow = 0u;
		fsmInfo->buttonToCheck = 0u;
		fsmInfo->currentState = FsmState_ShowSeqEnable;
	}
	else if (User_Win == fsmInfo->userChooseState)
	{
		fsmInfo->userChooseState = User_NoReaction;

		if (fsmInfo->buttonToCheck >= fsmInfo->buttonsNumInSeq)
		{
			fsmInfo->currentState = FsmState_Win;
		}
		else
		{
			fsmInfo->currentState = FsmState_WaitForUser;
		}
	}
	else if(User_Lose == fsmInfo->userChooseState)
	{
		fsmInfo->currentState = FsmState_Lose;
	}
}

void Fsm_Win(tFsmInfo * const fsmInfo)
{
	if (false == fsmTimer.isRunning)
	{
		fsmTimer.timeout = 1500;
		FsmTimer_StartTimer(&fsmTimer);

		fsmInfo->score++;
		Display_SetScore(fsmInfo->score);

		fsmInfo->currentRound++;
		Player_SetSongToPlay(Song_Win);
	}
}

void FsmTransition_Win(tFsmInfo * const fsmInfo)
{
	if (FsmTimer_IsTimerExpired(&fsmTimer))
	{
		fsmInfo->currentState = FsmState_GenerateSeq;
	}
}

void Fsm_Lose(tFsmInfo * const fsmInfo)
{
	if (false == fsmTimer.isRunning)
	{
		fsmTimer.timeout = 1500;
		FsmTimer_StartTimer(&fsmTimer);

		fsmInfo->score = 0u;
		fsmInfo->currentRound = 0u;
		Display_SetScore(fsmInfo->score);
		Player_SetSongToPlay(Song_Lose);
	}

	fsmInfo->score = 0u;
	Display_SetScore(fsmInfo->score);
}

void FsmTransition_Lose(tFsmInfo * const fsmInfo)
{
	if (FsmTimer_IsTimerExpired(&fsmTimer))
	{
		fsmInfo->currentState = FsmState_Init;
	}
}
