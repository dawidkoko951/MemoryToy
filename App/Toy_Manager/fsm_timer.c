/***********************************************************************
 * fsm_timer.c
 *
 *  Created on: 8 kwi 2021
 *      Author: Dawid
 **********************************************************************/

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include <stddef.h>
#include "fsm_timer.h"
#include "delay.h"

#include "logger.h"
/***********************************************************************
 * DEFINES
 **********************************************************************/

/***********************************************************************
 * TYPES
 **********************************************************************/

/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/

/***********************************************************************
 * LOCAL DATA
 **********************************************************************/

/***********************************************************************
 * PROTOTYPES
 **********************************************************************/

/***********************************************************************
 * FUNCTIONS
 **********************************************************************/
tFsmTimerResult FsmTimer_StartTimer(tFsmTimer * const timer_info)
{
	tFsmTimerResult result = Timer_NotOk;

	if (NULL != timer_info)
	{
		if (false == timer_info->isRunning)
		{
			timer_info->startTime = Delay_GetTicks();

			if (timer_info->timeout > 0)
			{
				timer_info->isRunning = true;
				result = Timer_Ok;
			}
		}
	}

	return result;
}

tBool FsmTimer_IsTimerExpired(tFsmTimer * const timer_info)
{
	tBool timer_expired = false;

	if (NULL != timer_info)
	{
		if (false == timer_info->isRunning)
		{
			timer_expired = true;
		}
		else
		{
			uint32_t curr_time = Delay_GetTicks();

			if ((curr_time - timer_info->startTime) >= timer_info->timeout)
			{
				timer_info->isRunning = false;
				timer_expired = true;
			}
		}
	}

	return timer_expired;
}

void FsmTimer_ResetTimer(tFsmTimer * const timer_info)
{
	if (NULL != timer_info)
	{
		timer_info->isRunning = false;
	}
}
