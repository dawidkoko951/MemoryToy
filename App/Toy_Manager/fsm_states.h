/***********************************************************************
 * fsm_states.h
 *
 *  Created on: 12 mar 2021
 *      Author: Dawid
 **********************************************************************/

#ifndef TOY_MANAGER_FSM_STATES_H_
#define TOY_MANAGER_FSM_STATES_H_

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include "fsm_base.h"

/***********************************************************************
 * DEFINES
 **********************************************************************/

/***********************************************************************
 * TYPES
 **********************************************************************/

/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/

/***********************************************************************
 * PROTOTYPES
 **********************************************************************/
void Fsm_Init(tFsmInfo * const fsmInfo);
void Fsm_Idle(tFsmInfo * const fsmInfo);
void Fsm_GenerateSeq(tFsmInfo * const fsmInfo);
void Fsm_ShowSeqEnable(tFsmInfo * const fsmInfo);
void Fsm_ShowSeqDisable(tFsmInfo * const fsmInfo);
void Fsm_WaitForUser(tFsmInfo * const fsmInfo);
void Fsm_Win(tFsmInfo * const fsmInfo);
void Fsm_Lose(tFsmInfo * const fsmInfo);

void FsmTransition_Init(tFsmInfo * const fsmInfo);
void FsmTransition_Idle(tFsmInfo * const fsmInfo);
void FsmTransition_GenerateSeq(tFsmInfo * const fsmInfo);
void FsmTransition_ShowSeqEnable(tFsmInfo * const fsmInfo);
void FsmTransition_ShowSeqDisable(tFsmInfo * const fsmInfo);
void FsmTransition_WaitForUser(tFsmInfo * const fsmInfo);
void FsmTransition_Win(tFsmInfo * const fsmInfo);
void FsmTransition_Lose(tFsmInfo * const fsmInfo);

#endif /* TOY_MANAGER_FSM_STATES_H_ */
