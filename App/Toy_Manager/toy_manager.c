/***********************************************************************
 * toy_manager.c
 *
 *  Created on: 11 mar 2021
 *      Author: Dawid
 **********************************************************************/

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include <stdint.h>
#include <stdlib.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "toy_manager.h"
#include "fsm_states.h"
#include "fsm_base.h"

#include "mp3player.h"
#include "display.h"
#include "buttons.h"
#include "led_manager.h"
#include "delay.h"

#include "uart.h"
#include "logger.h"
/***********************************************************************
 * DEFINES
 **********************************************************************/
#define LEDS_UPDATE_TIME_MS		(10)
/***********************************************************************
 * TYPES
 **********************************************************************/
typedef void (*tFsmFuncPtr)(tFsmInfo * const fsmInfo);
/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/

/***********************************************************************
 * LOCAL DATA
 **********************************************************************/
static const tFsmFuncPtr fsmStateHandler[] =
{
	[FsmState_Init] 			= Fsm_Init,
	[FsmState_Idle] 			= Fsm_Idle,
	[FsmState_GenerateSeq] 		= Fsm_GenerateSeq,
	[FsmState_ShowSeqEnable]	= Fsm_ShowSeqEnable,
	[FsmState_ShowSeqDisable]	= Fsm_ShowSeqDisable,
	[FsmState_WaitForUser]		= Fsm_WaitForUser,
	[FsmState_Win]				= Fsm_Win,
	[FsmState_Lose]				= Fsm_Lose,
};

static const tFsmFuncPtr fsmTransitionHandler[] =
{
	[FsmState_Init] 			= FsmTransition_Init,
	[FsmState_Idle] 			= FsmTransition_Idle,
	[FsmState_GenerateSeq] 		= FsmTransition_GenerateSeq,
	[FsmState_ShowSeqEnable]	= FsmTransition_ShowSeqEnable,
	[FsmState_ShowSeqDisable]	= FsmTransition_ShowSeqDisable,
	[FsmState_WaitForUser]		= FsmTransition_WaitForUser,
	[FsmState_Win]				= FsmTransition_Win,
	[FsmState_Lose]				= FsmTransition_Lose,
};
/***********************************************************************
 * PROTOTYPES
 **********************************************************************/
static void InitializeFsm(tFsmInfo * const fsmInfo);
static void ReadUserInput(tFsmInfo * const fsmInfo);
static void SetOutput(tFsmInfo * const fsmInfo);
/***********************************************************************
 * FUNCTIONS
 **********************************************************************/
void ToyManager_Run(void)
{
	tFsmInfo fsm_info;

	Player_Init();
	Display_Init();
	Buttons_Init();
	LedManager_Init();
	Delay_Init();
	sei();

	InitializeFsm(&fsm_info);

	while(1)
	{
		ReadUserInput(&fsm_info);

		fsmStateHandler[fsm_info.currentState](&fsm_info);
		fsmTransitionHandler[fsm_info.currentState](&fsm_info);

		SetOutput(&fsm_info);
	}
}

static void InitializeFsm(tFsmInfo * const fsmInfo)
{
	fsmInfo->currentState = FsmState_Init;
	fsmInfo->funcButton.startButton.current_state 	= State_Released;
	fsmInfo->funcButton.startButton.previous_state 	= State_Released;
	fsmInfo->funcButton.repeatButton.current_state 	= State_Released;
	fsmInfo->funcButton.repeatButton.previous_state = State_Released;

	fsmInfo->colorButton.blueButton.current_state 	= State_Released;
	fsmInfo->colorButton.blueButton.previous_state 	= State_Released;
	fsmInfo->colorButton.redButton.current_state 	= State_Released;
	fsmInfo->colorButton.redButton.previous_state   = State_Released;
	fsmInfo->colorButton.whiteButton.current_state 	= State_Released;
	fsmInfo->colorButton.whiteButton.previous_state = State_Released;
	fsmInfo->colorButton.greenButton.current_state 	= State_Released;
	fsmInfo->colorButton.greenButton.previous_state = State_Released;

	for (uint8_t idx = 0u; idx < COLOR_BUTTONS_NUM; idx++)
	{
		fsmInfo->buttonLed[idx] = colorButtons[idx].led;
		fsmInfo->buttonSong[idx] = colorButtons[idx].song;
	}

	fsmInfo->currentRound = 1u;
	fsmInfo->score = 0u;
	Display_SetScore(fsmInfo->score);
	fsmInfo->buttonToShow = 0u;
	fsmInfo->buttonToCheck = 0u;
	fsmInfo->lastPressedColorBtn = NO_PRESSED_BUTTON;
	fsmInfo->userChooseState = User_NoReaction;
}

static void ReadUserInput(tFsmInfo * const fsmInfo)
{
	fsmInfo->funcButton.startButton.current_state  = Buttons_GetState(Button_Start);
	fsmInfo->funcButton.repeatButton.current_state = Buttons_GetState(Button_Repeat);

	fsmInfo->colorButton.blueButton.current_state  = Buttons_GetState(Button_Blue);
	fsmInfo->colorButton.redButton.current_state   = Buttons_GetState(Button_Red);
	fsmInfo->colorButton.whiteButton.current_state = Buttons_GetState(Button_White);
	fsmInfo->colorButton.greenButton.current_state = Buttons_GetState(Button_Green);
}

static void SetOutput(tFsmInfo * const fsmInfo)
{
	uint32_t curr_ticks = Delay_GetTicks();
	static uint32_t leds_upd_time = 0U;

	if ((curr_ticks - leds_upd_time) >= LEDS_UPDATE_TIME_MS)
	{
		LedManager_UpdateMap();
		leds_upd_time = curr_ticks;
	}

	UpdateButtonState(&fsmInfo->funcButton.startButton);
	UpdateButtonState(&fsmInfo->funcButton.repeatButton);

	UpdateButtonState(&fsmInfo->colorButton.blueButton);
	UpdateButtonState(&fsmInfo->colorButton.redButton);
	UpdateButtonState(&fsmInfo->colorButton.whiteButton);
	UpdateButtonState(&fsmInfo->colorButton.greenButton);

	if (0xFF != Player_GetSongToPlay())
	{
		Player_PlayAudio(Player_GetSongToPlay());
		Player_SetSongToPlay(0xFF);
	}
}


