/***********************************************************************
 * display.c
 *
 *  Created on: 27 sty 2021
 *      Author: Dawid
 **********************************************************************/

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <math.h>
#include "display.h"
#include "timer.h"
#include "common.h"

/***********************************************************************
 * DEFINES
 **********************************************************************/
#define SEG_A_PIN		PD7
#define SEG_A_PORT		(volatile uint8_t*)(0x0B + 0x20)	// PORTD
#define SEG_A_DDR		(volatile uint8_t*)(0x0A + 0x20)	// DDRD

#define SEG_B_PIN		PB0
#define SEG_B_PORT		(volatile uint8_t*)(0x05 + 0x20)	// PORTB
#define SEG_B_DDR		(volatile uint8_t*)(0x04 + 0x20)	// DDRB

#define SEG_C_PIN		PB3
#define SEG_C_PORT		(volatile uint8_t*)(0x05 + 0x20)	// PORTB
#define SEG_C_DDR		(volatile uint8_t*)(0x04 + 0x20)	// DDRB

#define SEG_D_PIN		PB2
#define SEG_D_PORT		(volatile uint8_t*)(0x05 + 0x20)	// PORTB
#define SEG_D_DDR		(volatile uint8_t*)(0x04 + 0x20)	// DDRB

#define SEG_E_PIN		PB1
#define SEG_E_PORT		(volatile uint8_t*)(0x05 + 0x20)	// PORTB
#define SEG_E_DDR		(volatile uint8_t*)(0x04 + 0x20)	// DDRB

#define SEG_F_PIN		PD5
#define SEG_F_PORT		(volatile uint8_t*)(0x0B + 0x20)	// PORTD
#define SEG_F_DDR		(volatile uint8_t*)(0x0A + 0x20)	// DDRD

#define SEG_G_PIN		PD6
#define SEG_G_PORT		(volatile uint8_t*)(0x0B + 0x20)	// PORTD
#define SEG_G_DDR		(volatile uint8_t*)(0x0A + 0x20)	// DDRD

#define DIGIT_1_PIN		PD3
#define DIGIT_1_PORT	(volatile uint8_t*)(0x0B + 0x20)	// PORTD
#define DIGIT_1_DDR		(volatile uint8_t*)(0x0A + 0x20)	// DDRD
#define DIGIT_1_IDX		(0U)

#define DIGIT_2_PIN		PD4
#define DIGIT_2_PORT	(volatile uint8_t*)(0x0B + 0x20)	// PORTD
#define DIGIT_2_DDR		(volatile uint8_t*)(0x0A + 0x20)	// DDRD
#define DIGIT_2_IDX		(1U)

#define SEGMENT_NUM		(7U)
#define DIGIT_NUM		(2U)
#define HIGHEST_NUMBER	(9U)


#define TIMER_IRQ_FREQ	((uint16_t)(100U * DIGIT_NUM))	/* 100Hz for one digit */

/***********************************************************************
 * TYPES
 **********************************************************************/
typedef struct
{
	volatile uint8_t* port;
	uint8_t pin;
}tDisplayPinInfo;
/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/
const tDisplayPinInfo segmentTable[SEGMENT_NUM] =
{
	{SEG_A_PORT, SEG_A_PIN},
	{SEG_B_PORT, SEG_B_PIN},
	{SEG_C_PORT, SEG_C_PIN},
	{SEG_D_PORT, SEG_D_PIN},
	{SEG_E_PORT, SEG_E_PIN},
	{SEG_F_PORT, SEG_F_PIN},
	{SEG_G_PORT, SEG_G_PIN}
};

const tDisplayPinInfo digitTable[DIGIT_NUM] =
{
	{DIGIT_1_PORT, DIGIT_1_PIN},
	{DIGIT_2_PORT, DIGIT_2_PIN}
};

/* Segment configuration for all digits */
const uint8_t segNumberConfig[] =
{
	0b0111111,	/* 0 on display */
	0b0000110,	/* 1 on display */
	0b1011011,	/* 2 on display */
	0b1001111,	/* 3 on display */
	0b1100110,	/* 4 on display */
	0b1101101,	/* 5 on display */
	0b1111101,	/* 6 on display */
	0b0000111,	/* 7 on display */
	0b1111111,	/* 8 on display */
	0b1101111 	/* 9 on display */
};
/***********************************************************************
 * LOCAL DATA
 **********************************************************************/
static volatile uint8_t digitsValue[DIGIT_NUM] = {0};

/***********************************************************************
 * PROTOTYPES
 **********************************************************************/
static void SetSegment(volatile uint8_t * port, uint8_t pin, tState state);
static void SetDigit(volatile uint8_t * port, uint8_t pin, tState state);
static void DisplayNumber(uint8_t digit, uint8_t num);
static void TimerInit(void);
static void TimerISR(void);

/***********************************************************************
 * FUNCTIONS
 **********************************************************************/
void Display_Init(void)
{
	*(SEG_A_DDR) |= (1 << SEG_A_PIN);
	*(SEG_B_DDR) |= (1 << SEG_B_PIN);
	*(SEG_C_DDR) |= (1 << SEG_C_PIN);
	*(SEG_D_DDR) |= (1 << SEG_D_PIN);
	*(SEG_E_DDR) |= (1 << SEG_E_PIN);
	*(SEG_F_DDR) |= (1 << SEG_F_PIN);
	*(SEG_G_DDR) |= (1 << SEG_G_PIN);

	*(DIGIT_1_DDR) |= (1 << DIGIT_1_PIN);
	*(DIGIT_2_DDR) |= (1 << DIGIT_2_PIN);


	for (uint8_t idx = 0U; idx < SEGMENT_NUM; idx++)
	{
		SetSegment(segmentTable[idx].port, segmentTable[idx].pin, DISABLE);
	}

	for (uint8_t idx = 0U; idx < DIGIT_NUM; idx++)
	{
		SetDigit(digitTable[idx].port, digitTable[idx].pin, DISABLE);
	}

	TimerInit();
}

void Display_SetScore(uint16_t score)
{
	/* All digits but last */
	for (uint8_t idx = DIGIT_NUM; idx > 0U; idx--)
	{
		digitsValue[idx - 1] = (uint8_t)(score / pow(10, idx - 1));
	}

	/* Last digit */
	digitsValue[0] = score % 10;
}

static void SetSegment(volatile uint8_t * port, uint8_t pin, tState state)
{
	switch(state)
	{
		case DISABLE:
		{
			*(port) |= (1 << pin);
			break;
		}
		case ENABLE:
		{
			*(port) &= ~(1 << pin);
			break;
		}
		default:
		{
			break;
		}
	}
}

static void SetDigit(volatile uint8_t * port, uint8_t pin, tState state)
{
	switch(state)
	{
		case DISABLE:
		{
			*(port) &= ~(1 << pin);
			break;
		}
		case ENABLE:
		{
			*(port) |= (1 << pin);
			break;
		}
		default:
		{
			break;
		}
	}
}

static void DisplayNumber(uint8_t digit, uint8_t num)
{
	/* Firstly disable all digits */
	for (uint8_t digit_idx = 0u; digit_idx < DIGIT_NUM; digit_idx++)
	{
		SetDigit(digitTable[digit_idx].port, digitTable[digit_idx].pin, DISABLE);
	}

	if ((digit < DIGIT_NUM) && (num <= HIGHEST_NUMBER))
	{
		SetDigit(digitTable[digit].port, digitTable[digit].pin, ENABLE);

		for (uint8_t idx = 0u; idx < SEGMENT_NUM; idx++)
		{
			tState state = (tState)((segNumberConfig[num] & (1 << idx)) == (1 << idx));
			SetSegment(segmentTable[idx].port, segmentTable[idx].pin, state);
		}
	}
}

static void TimerInit(void)
{
	tTimerInfo timer_A;

	timer_A.channel = Timer_A;
	timer_A.isr_freq = TIMER_IRQ_FREQ;
	timer_A.isr_callback = TimerISR;

	Timer_InitializeTim1(&timer_A);
}

static void TimerISR(void)
{
	static uint8_t digit_idx = 0u;

	/* Disable all digits */
	for (uint8_t idx = 0u; idx < DIGIT_NUM; idx++)
	{
		SetDigit(digitTable[idx].port, digitTable[idx].pin, DISABLE);
	}

	if (digit_idx >= DIGIT_NUM)
	{
		digit_idx = 0u;
	}

	DisplayNumber(digit_idx, digitsValue[digit_idx]);
	digit_idx++;
}
