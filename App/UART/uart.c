/***********************************************************************
 * uart.c
 *
 *  Created on: 14 sty 2021
 *      Author: Dawid
 **********************************************************************/

/***********************************************************************
 * INCLUDES
 **********************************************************************/
#include <avr/io.h>
#include <stdint.h>
#include <stdlib.h>

/***********************************************************************
 * DEFINES
 **********************************************************************/
#define BAUD		((uint32_t)9600)

/***********************************************************************
 * TYPES
 **********************************************************************/

/***********************************************************************
 * GLOBAL DATA
 **********************************************************************/

/***********************************************************************
 * LOCAL DATA
 **********************************************************************/

/***********************************************************************
 * PROTOTYPES
 **********************************************************************/
static void Uart_SendOneByte(uint8_t value);

/***********************************************************************
 * FUNCTIONS
 **********************************************************************/
void Uart_Init(void)
{
	const uint16_t ubrr_value = F_CPU/((uint32_t)16 * BAUD) - (uint32_t)1U;
	UBRR0 = (uint16_t)ubrr_value;

	UCSR0B |= (1 << TXEN0) | (1 << RXEN0);
}

void Uart_Write(uint8_t* buffer, uint32_t size)
{
	for (uint8_t idx = 0U; idx < size; idx++)
	{
		Uart_SendOneByte(buffer[idx]);
	}
}

static void Uart_SendOneByte(uint8_t value)
{
	while(!(UCSR0A & (1 << UDRE0)));
	UDR0 = value;
}

